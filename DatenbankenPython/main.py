from flask import Flask, render_template, request
from connection import *

app = Flask(__name__)

@app.route('/')
def home():
    return render_template("home.html")


@app.route('/kurse')
def kurse():
    header_seminar = [i[1] for i in get_table_info("Seminar")]
    data = get_posts("Seminar")
    return render_template("kurse.html", data=data, header_seminar=header_seminar)


@app.route("/anmelden")
def anmelden():
    return render_template("anmelden.html")


@app.route("/angemeldet", methods=["POST"])
def angemeldet():
    svn = request.form.get("svn")
    first_name = request.form.get("first_name")
    last_name = request.form.get("last_name")
    plz = request.form.get("plz")
    ort = request.form.get("ort")
    straße = request.form.get("straße")
    hnr = request.form.get("hnr")
    telefon = request.form.get("telnr")


    if not svn or not first_name or not last_name or not plz or not ort or not straße or not hnr or not telefon:
        error_message = "Es sind alle Felder erforderlich"
        return render_template("anmeldung_fail.html",
                               svn=svn,
                               first_name=first_name,
                               last_name=last_name,
                               plz=plz,
                               ort=ort,
                               straße=straße,
                               hnr=hnr,
                               telnr=telefon,
                               error_message=error_message)
    try:
        add_to_person(svn, first_name, last_name, plz, ort, straße, hnr, telefon)
        return render_template("angemeldet.html")
    except sqlite3.IntegrityError:
        error_message = "Die angegebene Sozialversicherungsnummer wurde bereits registriert."
        return render_template("anmeldung_fail.html",
                               svn=svn,
                               first_name=first_name,
                               last_name=last_name,
                               plz=plz,
                               ort=ort,
                               straße=straße,
                               hnr=hnr,
                               telnr=telefon,
                               error_message=error_message)


@app.route("/kursanmeldung")
def kursanmeldung():
    return render_template("kursanmeldung.html")


@app.route("/datenaenderung", methods=["POST"])
def datenaenderung():
    return render_template("datenaenderung.html")


@app.route("/geaendert", methods=["POST"])
def geaendert():
    svn = request.form.get("svn")
    first_name = request.form.get("first_name")
    last_name = request.form.get("last_name")
    plz = request.form.get("plz")
    ort = request.form.get("ort")
    straße = request.form.get("straße")
    hnr = request.form.get("hnr")

    if not svn:
        error_message='Die Sozialversicherungsnummer ist für die Änderung erforderlich'
        return render_template("datenaenderung.html",
                               svn=svn,
                               first_name=first_name,
                               last_name=last_name,
                               plz=plz,
                               ort=ort,
                               straße=straße,
                               hnr=hnr,
                               error_message=error_message)
    try:
        change_person(svn, first_name, last_name, plz, ort, straße, hnr)
        return render_template("geaendert.html")
    except sqlite3.IntegrityError:
        error_message = "Die angegebene Sozialversicherungsnummer wurde bereits registriert."
        return render_template("anmeldung_fail.html",
                               svn=svn,
                               first_name=first_name,
                               last_name=last_name,
                               plz=plz,
                               ort=ort,
                               straße=straße,
                               hnr=hnr,
                               error_message=error_message)


@app.route("/organisator")
def about():
    return render_template("organisator.html")


@app.route("/organisator_angemeldet", methods=["POST"])
def organisator_angemeldet():
    svn = request.form.get("svn")
    last_name = request.form.get("last_name")

    if check_organisator(svn, last_name):
        return render_template("organisator_angemeldet.html")

    else:
        error_message="Anmeldedaten inkorrekt"
        return render_template("organisator.html", error_message=error_message)


@app.route("/addorganisator", methods=["POST"])
def addorganisator():
    return render_template("addorganisator.html")

@app.route("/addorganisator2", methods=["POST"])
def addorganisator2():
    neuer_organisator = int(request.form.get("org"))
    success_message = f"Neuer Organisator mit der Angestellten-Nr. {neuer_organisator} hinzugefügt"
    error_message = "Angestellten-Nr entweder nicht vergeben oder bereits Organisator"
    if neuer_organisator in [i[0] for i in get_column("Organisator", "AngestelltenNr")] or not\
        neuer_organisator in [i[0] for i in get_column("Angestellter", "AngestelltenNr")]:
        return render_template("addorganisator.html", error_message=error_message)
    else:
        add_organisator(neuer_organisator)
        return render_template("addorganisator.html", success_message=success_message)
