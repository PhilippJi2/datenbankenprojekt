import sqlite3

db = "Ausbildungsbetrieb.db"


def get_table_info(Table):
    sqliteConnection = sqlite3.connect(db)
    cursor = sqliteConnection.cursor()
    cursor.execute(f"""PRAGMA table_info({Table}) """)
    return cursor.fetchall()


def get_column(table, column):
    sqliteConnection = sqlite3.connect(db)
    cursor = sqliteConnection.cursor()
    cursor.execute(f"""SELECT {column} FROM {table}""")
    return cursor.fetchall()


def get_posts(table):
    sqliteConnection = sqlite3.connect(db)
    cursor = sqliteConnection.cursor()
    cursor.execute(f"""SELECT * FROM {table}""")
    return cursor.fetchall()


def add_to_person(svn, vor, nach, plz, ort, str, hnr, telnr):
    sqliteConnection = sqlite3.connect(db)
    cursor = sqliteConnection.cursor()
    cursor.execute(f"""INSERT INTO Person (SVN,Vorname,Nachname,PLZ,Ort,Strasse,Hausnr)
    VALUES("{svn}" , "{vor}", "{nach}", "{plz}", "{ort}","{str}","{hnr}")""")
    cursor.execute(f"""INSERT INTO Telefonnummer (Person , TelNr)
    VALUES("{svn}" , "{telnr}")""")
    sqliteConnection.commit()


def change_person(svn, vor=0, nach=0, plz=0, ort=0, str=0, hnr=0):
    sqliteConnection = sqlite3.connect(db)
    cursor = sqliteConnection.cursor()
    header_seminar = [i[1] for i in get_table_info("Person")]
    header_seminar.remove("SVN")
    values = [vor, nach, plz, ort, str, hnr]
    for i in range(len(values)):
        if values[i]:
            cursor.execute(f"""
            UPDATE Person
            SET "{header_seminar[i]}" = "{values[i]}"
            where SVN = "{svn}"
            """)
    sqliteConnection.commit()


def check_organisator(svn,nach):
    sqliteConnection = sqlite3.connect(db)
    cursor = sqliteConnection.cursor()
    cursor.execute(f"""
    SELECT AngestelltenNr
    FROM Organisator 
    WHERE EXISTS
    (SELECT AngestelltenNr FROM Angestellter WHERE SVN = "{svn}")
    """)
    organisator = cursor.fetchall()
    cursor.execute(f"""
    SELECT Nachname 
    FROM Person
    WHERE SVN = "{svn}"
    """)
    nachname = cursor.fetchall()
    if not nachname:
        return False
    elif organisator and nachname[0][0]==nach:
        return True
    else:
        return False

def add_organisator(nr):
    sqliteConnection = sqlite3.connect(db)
    cursor = sqliteConnection.cursor()
    cursor.execute(f"""
    SELECT * from Angestellter where AngestelltenNr = "{nr}"
    """)
    mitarbeiter = cursor.fetchall()
    if mitarbeiter:
        cursor.execute(f"""
        INSERT INTO Organisator (AngestelltenNr)
        VALUES("{nr}")
        """)
        sqliteConnection.commit()
    else:
        pass



